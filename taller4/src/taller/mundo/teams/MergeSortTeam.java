package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
     public MergeSortTeam()
     {
          super("Merge sort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
     {
          return merge_sort(lista, orden);
     }


     private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
     {
    	 // Trabajo en Clase
    	 Comparable[] aux1 = new Comparable[lista.length/2];
    	 Comparable[] aux2 = new Comparable[lista.length-aux1.length];
    	 System.arraycopy(lista, 0, aux1, 0, aux1.length);
         System.arraycopy(lista, aux1.length, aux2, 0, aux2.length);
         if(aux1.length>1){
        	 merge_sort(aux1,orden);
         }
         if(aux2.length>1){
        	 merge_sort(aux2,orden);
         }
    	 lista = merge(aux1,aux2,orden);
         return lista;
     }

     private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden)
     {	
    	Comparable[] aux = new Comparable[izquierda.length+derecha.length];
    	int izq =0;
    	int der = 0;
    	int k = 0;
    	 while(izq<izquierda.length&&der<derecha.length){
    		 
    			
    			 if(izquierda[izq].compareTo(derecha[der])<0){
    				 aux[k++]=izquierda[izq++];
    			 }else{
    				 aux[k++]=derecha[der++];
    			 }
    			 
    	 }		 
    		 
    	 while(izq<izquierda.length){
    		 aux[k++]=izquierda[izq++];
    	 }
    	 while(der<derecha.length){
    		 aux[k++]=derecha[der++];
    	 }
    	 return aux;
     }


}
