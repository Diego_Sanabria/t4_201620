package taller.mundo.teams;

/*
 * QuickSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import java.util.Random;

import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class QuickSortTeam extends AlgorithmTeam
{

     private static Random random = new Random();

     public QuickSortTeam()
     {
          super("Quicksort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
     {
          quicksort(list, 0, list.length-1, orden);
          return list;
     }
        // Trabajo en Clase
     
     private static void quicksort(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
     {
    	 if(fin<=inicio)return;
    	 int j = particion(lista, inicio, fin, orden);
    	 quicksort(lista,inicio,j-1,orden);
    	 quicksort(lista,j+1,fin,orden);
    	 // Trabajo en Clase
     }

    private static int particion(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
    {
    	int i = inicio;
		int j = fin+1;
		Comparable v = lista[inicio];
    		
    	if(orden==TipoOrdenamiento.ASCENDENTE){	
    		while(true){
    			while (lista[++i].compareTo(v)<0){
    				
    				if(i==fin){
    					break;
    				}
    			}
    			while (lista[--j].compareTo(v)>0){
    				
    				if(j==inicio){
    					break;
    				}
    			}
    			
    			if(i>=j){
    				break;
    			}
    			
    				Comparable temp = lista[i];
    				lista[i]= lista[j];
    				lista[j]=temp;
    				
    				
    			
    			
    		}
    	Comparable temp = lista[inicio];
    	lista[inicio]=lista[j];
    	lista[j]=temp;
    	return j;
    	}
    	else{
    		while(true){
    			while (lista[++i].compareTo(v)>0){
    				
    				if(i==fin){
    					break;
    				}
    			}
    			while (lista[--j].compareTo(v)<0){
    				
    				if(j==inicio){
    					break;
    				}
    			}
    			
    			if(i>=j){
    				break;
    			}
    			
    				Comparable temp = lista[i];
    				lista[i]= lista[j];
    				lista[j]=temp;
    				
    				
    			
    			
    		}
    	Comparable temp = lista[inicio];
    	lista[inicio]=lista[j];
    	lista[j]=temp;
    	return j;
    	}
    }

    private static int eleccionPivote(int inicio, int fin)
    {
         /**
           Este procedimiento realiza la elecci�n de un �ndice que corresponde al pivote res-
           pecto al cual se realizar�  la partici�n de la lista. Se recomienda escoger el ele-
           mento que se encuentra en la mitad, o de forma aleatoria entre los �ndices [inicio, fin).
         **/
    	
    	
         return randInt(inicio,fin);
    }

    /**
      Retorna un número aleatorio que se encuentra en el intervalo [min, max]; inclusivo.
      @param min, índice inicial del intervalo.
      @param max, índice final del intervalo.
      @return Un número aleatorio en el intervalo [min, max].
    **/
    public static int randInt(int min, int max) 
    {
          int randomNum = random.nextInt((max - min) + 1) + min;
          return randomNum;
    }
    // Trabajo en Clase

}
